package main

import (
	"crypto/aes"
	"crypto/cipher"
	cryptorand "crypto/rand"
	"encoding/base64"
	"fmt"
	"sync"
)

// Wire format of a trusted device token key:
//
// +------------------+------------------+----------------------------------------------+
// | ID               | Nonce            | Key                                          |
// | (16B)            | (16B)            | (44B)                                        |
// +------------------+------------------+----------------------------------------------+
// | zAStuYdzxWbeJQn+ | tqckbcM5qqyxztrh | wWLVwrNSjqr6lY2twnEO1yTVNuUDXkq+PAdEaw9mRqE= |
// +------------------+------------------+----------------------------------------------+
type tdtKey struct {
	ID    string // An internal identifier for the token. It is stored in base64 encoding (16 bytes).
	Nonce []byte // The nonce used for encryption (12 bytes).
	Key   []byte // The encryption key (32 bytes). It is 32 bytes because we use AES-256-GCM.
}

// Marshal base64 encodes the internal data structure so it can be used as a
// password and sent over the wire in a RADIUS packet.
func (t tdtKey) Marshal() []byte {
	b := make([]byte, 76)
	copy(b[:16], t.ID)
	if t.Nonce == nil {
		t.Nonce = make([]byte, 12)
	}
	if t.Key == nil {
		t.Key = make([]byte, 32)
	}
	base64.StdEncoding.Encode(b[16:32], t.Nonce)
	base64.StdEncoding.Encode(b[32:76], t.Key)
	return b
}

// Unmarshal decodes a base64 encoded byte slice into the internal data.
func (t *tdtKey) Unmarshal(b []byte) error {
	if len(b) != 76 {
		return fmt.Errorf("trusted device token key: invalid length")
	}
	t.ID = string(b[:16]) // Can be copied 1:1.
	// Decode the nonce and key from base64.
	t.Nonce = make([]byte, 12)
	if _, err := base64.StdEncoding.Decode(t.Nonce, b[16:32]); err != nil {
		return fmt.Errorf("trusted device token key: error decoding nonce: %w", err)
	}
	t.Key = make([]byte, 32)
	if _, err := base64.StdEncoding.Decode(t.Key, b[32:76]); err != nil {
		return fmt.Errorf("trusted device token key: error decoding key: %w", err)
	}
	return nil
}

// An encryptionStore is an interface for storing and retrieving encrypted
// trusted device tokens.
type encryptionStore interface {
	// Get decrypts and returns a trusted device token by id.
	Get(key tdtKey) ([]byte, error)
	// Put encrypts and stores a trusted device token. It returns the
	// (generated) encryption key used for the token and a token id for later
	// decryption.
	Put(token []byte) (key tdtKey, err error)
	// Delete removes a trusted device token by id. The key is not used for this
	// operation.
	Delete(id string) error
}

type codec interface {
	// Encrypt encrypts a trusted device token.
	Encrypt(token []byte) (key tdtKey, ciphertext []byte, err error)
	// Decrypt decrypts a trusted device token.
	Decrypt(key, nonce, ciphertext []byte) (token []byte, err error)
}

// inMemoryEncryptionStore is an in-memory implementation of the
// encryptionStore. In the future we should probably use redis or something.
type inMemoryEncryptionStore struct {
	mux   sync.RWMutex
	store map[string][]byte
	aead  codec
}

func newMemoryEncryptionStore() *inMemoryEncryptionStore {
	return &inMemoryEncryptionStore{
		store: make(map[string][]byte),
		aead:  &aesgcmCodec{},
	}
}

type aesgcmCodec struct{}

func (c *aesgcmCodec) Encrypt(token []byte) (key tdtKey, ciphertext []byte, err error) {
	// Generate a 32-byte key.
	key.Key = make([]byte, 32)
	if _, err := cryptorand.Read(key.Key); err != nil {
		return tdtKey{}, nil, fmt.Errorf("error generating encryption key: %w", err)
	}
	// Generate a 12-byte nonce.
	key.Nonce = make([]byte, 12)
	if _, err := cryptorand.Read(key.Nonce); err != nil {
		return tdtKey{}, nil, fmt.Errorf("error generating nonce: %w", err)
	}
	// Encrypt the token using the 32-byte key with AES-256-GCM.
	block, err := aes.NewCipher(key.Key)
	if err != nil {
		return tdtKey{}, nil, fmt.Errorf("error initializing block cipher: %w", err)
	}
	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return tdtKey{}, nil, fmt.Errorf("error initializing GCM: %w", err)
	}
	ciphertext = aesgcm.Seal(nil, key.Nonce, token, nil)
	return key, ciphertext, nil
}

func (c *aesgcmCodec) Decrypt(key, nonce, ciphertext []byte) (token []byte, err error) {
	// Decrypt the token using the 32-byte key with AES-256-GCM.
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, fmt.Errorf("error initializing block cipher: %w", err)
	}
	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, fmt.Errorf("error initializing GCM: %w", err)
	}
	plain, err := aesgcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		return nil, fmt.Errorf("error decrypting token: %w", err)
	}
	return plain, nil
}

func (s *inMemoryEncryptionStore) Get(key tdtKey) ([]byte, error) {
	s.mux.RLock()
	defer s.mux.RUnlock()
	// Check if the key exists.
	token, ok := s.store[key.ID]
	if !ok {
		return nil, fmt.Errorf("trusted device token key not found")
	}
	// Decrypt the token using the 32-byte key with AES-256-GCM.
	return s.aead.Decrypt(key.Key, key.Nonce, token)
}

func (s *inMemoryEncryptionStore) Put(token []byte) (tdtKey, error) {
	s.mux.Lock()
	defer s.mux.Unlock()
	out, ciphertext, err := s.aead.Encrypt(token)
	if err != nil {
		return tdtKey{}, fmt.Errorf("error encrypting token: %w", err)
	}
	// Generate a random id for the token.
	id := make([]byte, 12)
	if _, err := cryptorand.Read(id); err != nil {
		return tdtKey{}, fmt.Errorf("error generating id: %w", err)
	}
	out.ID = base64.StdEncoding.EncodeToString(id) // Always results in 16 bytes.
	// Store the encrypted token.
	s.store[out.ID] = ciphertext
	return out, nil
}

func (s *inMemoryEncryptionStore) Delete(id string) error {
	s.mux.Lock()
	defer s.mux.Unlock()
	delete(s.store, id)
	return nil
}

// Prove that inMemoryEncryptionStore implements the encryptionStore interface.
var _ encryptionStore = &inMemoryEncryptionStore{}
