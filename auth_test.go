package main

import "testing"

func TestPotentialTrustedDeviceToken(t *testing.T) {
	tests := []struct {
		name     string
		passcode string
		want     bool
	}{
		{
			name:     "passcode with numbers only",
			passcode: "123456",
			want:     false,
		},
		{
			name:     "passcode with non-numeric characters",
			passcode: "1234a6",
			want:     true,
		},
		{
			name:     "trusted device token",
			passcode: "1234567890",
			want:     true,
		},
		{
			name:     "long passcode",
			passcode: "123456789",
			want:     false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := potentialTrustedDeviceToken(tt.passcode); got != tt.want {
				t.Errorf("potentialTrustedDeviceToken() = %v, want %v", got, tt.want)
			}
		})
	}
}
