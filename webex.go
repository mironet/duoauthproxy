package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/mironet/duoauthproxy/client"
)

const (
	webexMessagesEndpoint = "v1/messages"
	webexAPIBaseURL       = "https://webexapis.com/"
)

type webexMsg struct {
	RoomID        string            `json:"roomId,omitempty"`
	ToPersonID    string            `json:"toPersonId,omitempty"`
	ToPersonEmail string            `json:"toPersonEmail,omitempty"`
	Text          string            `json:"text,omitempty"`
	Markdown      string            `json:"markdown,omitempty"`
	Files         []string          `json:"files,omitempty"`
	Attachments   []WebexAttachment `json:"attachments,omitempty"`
}

// WebexAttachment is used for adaptive cards.
type WebexAttachment struct {
	ContentType string      `json:"contentType,omitempty"`
	Content     interface{} `json:"content,omitempty"`
}

type webexCli struct {
	cli *client.Client
}

func newWebexCli(token string) (*webexCli, error) {
	cli, err := client.New(
		client.WithAuthorization(token),
		client.WithBaseURL(webexAPIBaseURL),
	)
	if err != nil {
		return nil, fmt.Errorf("error creating webex client: %w", err)
	}
	we := &webexCli{
		cli: cli,
	}
	return we, nil
}

// Send a text message to a person.
func (w *webexCli) sendPlainMessage(ctx context.Context, toPersonEmail, msg string) error {
	return w.sendMessage(ctx, webexMsg{
		ToPersonEmail: toPersonEmail,
		Text:          msg,
	})
}

func (w *webexCli) sendMessage(ctx context.Context, msg webexMsg) error {
	// First we marshal the msg to a JSON string.
	b, err := json.Marshal(msg)
	if err != nil {
		return fmt.Errorf("error marshaling webex message: %w", err)
	}
	// Then we create a new http request.
	req, err := w.cli.NewRequest(ctx, http.MethodPost, webexMessagesEndpoint, bytes.NewBuffer(b))
	if err != nil {
		return fmt.Errorf("error creating webex request: %w", err)
	}

	resp, err := w.cli.Do(ctx, req, nil)
	if err != nil {
		return fmt.Errorf("error sending webex message to %s: %w", msg.ToPersonEmail, err)
	}
	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusAccepted {
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			return fmt.Errorf("error reading webex response body: %w", err)
		}
		defer resp.Body.Close()
		return fmt.Errorf("unexpected status code %d from webex: %s", resp.StatusCode, string(body))
	}
	return nil
}
