package main

import (
	"context"
	cryptorand "crypto/rand"
	"encoding/base64"
	"fmt"

	"github.com/go-redis/redis/v8"
)

type redisEncryptionStore struct {
	client *redis.Client
	aead   codec
}

func newRedisEncryptionStore(ctx context.Context, addr, password string) (*redisEncryptionStore, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: password,
	})
	if err := client.Ping(ctx).Err(); err != nil {
		return nil, fmt.Errorf("error connecting to redis: %w", err)
	}
	return &redisEncryptionStore{
		client: client,
		aead:   &aesgcmCodec{},
	}, nil
}

func (s *redisEncryptionStore) Get(key tdtKey) ([]byte, error) {
	// Check if the key exists.
	token, err := s.client.Get(context.Background(), key.ID).Bytes()
	if err != nil {
		// If the key doesn't exist, return an error.
		if err == redis.Nil {
			return nil, fmt.Errorf("token not found")
		}
		return nil, fmt.Errorf("error getting token: %w", err)
	}
	// Decrypt the token using the 32-byte key with AES-256-GCM.
	return s.aead.Decrypt(key.Key, key.Nonce, token)
}

func (s *redisEncryptionStore) Put(token []byte) (tdtKey, error) {
	out, ciphertext, err := s.aead.Encrypt(token)
	if err != nil {
		return tdtKey{}, fmt.Errorf("error encrypting token: %w", err)
	}
	// Generate a random id for the token.
	id := make([]byte, 12)
	if _, err := cryptorand.Read(id); err != nil {
		return tdtKey{}, fmt.Errorf("error generating id: %w", err)
	}
	out.ID = base64.StdEncoding.EncodeToString(id) // Always results in 16 bytes.
	// Store the encrypted token.
	if err := s.client.Set(context.Background(), out.ID, ciphertext, 0).Err(); err != nil {
		return tdtKey{}, fmt.Errorf("error storing token: %w", err)
	}
	return out, nil
}

func (s *redisEncryptionStore) Delete(id string) error {
	if err := s.client.Del(context.Background(), id).Err(); err != nil {
		return fmt.Errorf("error deleting token: %w", err)
	}
	return nil
}

// Prove that redisEncryptionStore implements the encryptionStore interface.
var _ encryptionStore = &redisEncryptionStore{}
