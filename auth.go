package main

import (
	"context"
	"errors"
	"fmt"
	"net/url"
	"time"

	"github.com/Azure/azure-sdk-for-go/sdk/azcore/policy"
	"github.com/Azure/azure-sdk-for-go/sdk/azidentity"
	duoapi "github.com/duosecurity/duo_api_golang"
	"github.com/duosecurity/duo_api_golang/authapi"
)

const (
	duoApiUserAgent = "go-duo-api 1.0"
)

// This type of error can be passed on the clients. Other error types are logged
// but not passed on.
type errExternal struct {
	err    error
	reason string
}

func (e errExternal) Error() string {
	return e.err.Error()
}

func (e errExternal) Unwrap() error {
	return e.err
}

func (e errExternal) Reason() string {
	return e.reason
}

var errAuthDone = fmt.Errorf("authentication done, no error")

func newDuoClient() (*authapi.AuthApi, error) {
	cfg.RLock()
	defer cfg.RUnlock()
	if cfg.Duo.IntegrationKey == "" || cfg.Duo.SecretKey == "" || cfg.Duo.APIHostname == "" {
		return nil, fmt.Errorf("missing duo configuration")
	}
	duo := duoapi.NewDuoApi(cfg.Duo.IntegrationKey, cfg.Duo.SecretKey, cfg.Duo.APIHostname, duoApiUserAgent)
	api := authapi.NewAuthApi(*duo)
	return api, nil
}

// Check if the passcode is indeed a passcode (numbers only) or a trusted device
// token.
func potentialTrustedDeviceToken(passcode string) bool {
	// Bypass codes are 9 digits, anything longer could be a trusted device
	// token.
	if len(passcode) > 9 {
		return true
	}
	// Check if the passcode is a passcode (numbers only).
	for _, c := range passcode {
		if c < '0' || c > '9' {
			return true // It's a trusted device token.
		}
	}
	return false // It's a passcode.
}

const (
	duoAuthFactorPasscode = "passcode"
	duoAuthFactorPush     = "push"
)

// Authenticate the user with the DUO passcode. First check with the DUO API
// preauth feature if the user is allowed to authenticate. If yes, send the
// passcode to the DUO API for authentication.
func duoAuth(ctx context.Context, username, passcode, ipaddr string, saveToken *string) error {
	api, err := newDuoClient()
	if err != nil {
		return fmt.Errorf("duo: error creating duo client: %w", err)
	}
	// Check if we potentially have a trusted device token.
	var trustedDevice bool
	var factor = duoAuthFactorPasscode
	var preopts = []func(*url.Values){
		authapi.PreauthUsername(username),
		authapi.PreauthIpAddr(ipaddr),
	}
	if potentialTrustedDeviceToken(passcode) {
		// We have one.
		log.Debugf("🔑 got potential trusted device token for user %s", username)
		// Now fetch the trusted device token from the store.
		var tdt = &tdtKey{}
		if err := tdt.Unmarshal([]byte(passcode)); err != nil {
			return errExternal{
				err:    fmt.Errorf("duo: error unmarshaling trusted device token: %w", err),
				reason: "Trusted device token is invalid. Please use a passcode instead.",
			}
		}
		tok, err := store.Get(*tdt)
		if err != nil {
			return errExternal{
				err:    fmt.Errorf("duo: error getting trusted device token from store: %w", err),
				reason: "Trusted device token is invalid. Please use a passcode instead.",
			}
		}
		preopts = append(preopts, authapi.PreauthTrustedToken(string(tok)))
		trustedDevice = true
	}
	// Check if the user is allowed to authenticate.
	res, err := api.Preauth(preopts...)
	if err != nil {
		return fmt.Errorf("duo: error checking preauth: %w", err)
	}
	var opts = []func(*url.Values){
		authapi.AuthUsername(username),
		//authapi.AuthIpAddr(ipaddr), <-- This would have been awesome, but Meraki sends a weird (unrelated) IP address.
		//authapi.AuthAsync(), // <-- We keep it synchronous in this case since we don't have to wait for user input.
	}
	switch res.Response.Result {
	case "allow":
		// The user is not required to complete secondary authentication, either
		// because the user has "bypass" status or the effective policy for the
		// user's access of this application allows access without 2FA or Duo
		// enrollment. See the duo docs for more information:
		// https://duo.com/docs/authapi#/preauth
		log.Infof("🔓 user %s is allowed to authenticate without passing DUO", username)
		if trustedDevice {
			// In this case we auth, but by using DUO push.
			log.Infof("🔑 sending DUO push to user %s", username)
			factor = duoAuthFactorPush
			opts = append(opts, authapi.AuthDevice("auto"))
			break
		}
		return nil // Potentially dangerous if set this way in DUO.
	case "deny":
		// The user is not allowed to authenticate. Immediately deny access.
		log.Infof("🔒 user %s is not allowed to authenticate, denied by DUO", username)
		return errExternal{err: fmt.Errorf("duo: user %s is not allowed to authenticate, denied by DUO", username)}
	case "enroll":
		// The user is not enrolled in Duo and needs to enroll before. We don't
		// support this directly in this app.
		log.Infof("🔒 user %s is not enrolled in DUO", username)
		return errExternal{err: fmt.Errorf("duo: user %s is not enrolled in DUO", username)}
	case "auth":
		if trustedDevice {
			// Looks like the trusted device token was invalid/expired. Since we
			// have no passcode, we reply with an error.
			return errExternal{
				err:    fmt.Errorf("duo: trusted device token for user %s is invalid/expired", username),
				reason: res.Response.Status_Msg,
			}
		}
		// Continue with sending the passcode now.
		log.Infof("🔑 sending passcode to DUO for user %s", username)
		opts = append(opts, authapi.AuthPasscode(passcode))
	default:
		return fmt.Errorf("duo: unknown preauth result: %s", res.Response.Result)
	}

	// Preauth is done, now send the passcode or push request.
	authre, err := api.Auth(factor, opts...)
	if err != nil {
		return fmt.Errorf("duo: error trying auth factor %s: %w", factor, err)
	}
	switch authre.Response.Result {
	case "allow":
		// The user has successfully completed secondary authentication and is
		// allowed to access the application.
		log.Infof("🔓 user %s successfully authenticated with DUO", username)
		// If this was a push request, we can stop here because the user has
		// authenticated with the trusted device token.
		if factor == duoAuthFactorPush {
			return errAuthDone
		}
		// Set the passed trusted device token to save it for next time.
		log.Debugf("🔑 got trusted device token for user %s (len=%d)", username, len(authre.Response.Trusted_Device_Token))
		if saveToken != nil {
			*saveToken = authre.Response.Trusted_Device_Token
		}
		return nil
	case "deny":
		// The user has failed to complete secondary authentication and is not
		// allowed to access the application.
		log.Infof("🔒 user %s failed to authenticate with DUO, reason might be: %s: %s", username, authre.Response.Status, authre.Response.Status_Msg)
		return errExternal{
			err:    fmt.Errorf("DUO denied user %s", username),
			reason: fmt.Sprintf("status=%s, msg=%s", authre.Response.Status, authre.Response.Status_Msg),
		}
	default:
		return fmt.Errorf("duo: unknown auth result: %s, resp = %+v", authre.Response.Result, authre.Response)
	}
}

func authenticateUser(ctx context.Context, username, password, ipaddr string) (bool, error) {
	// Check DUO first.
	var trustedToken string
	// We pass a trusted device token in case the auth is successful. If the
	// auth is successful, we save the token and message it to the user so they
	// can use it next time.
	if err := duoAuth(ctx, username, password, ipaddr, &trustedToken); err != nil {
		if errors.Is(err, errAuthDone) {
			// This is not an error, it just means the user authenticated with a
			// trusted device token.
			return true, nil
		}
		// Everything else is an error.
		return false, fmt.Errorf("error authenticating user with DUO: %w", err)
	}

	// Start a device code login flow with Azure AD. Send the redirect URI over
	// Webex to the user in "username" in a private message.
	deviceCred, err := startAADDeviceCodeFlow(ctx, username, func(ctx context.Context, dcm azidentity.DeviceCodeMessage) error {
		msg := dcm.Message
		if dcm.UserCode != "" {
			log.Debugf("🔑 got user code %s for user %s", dcm.UserCode, username)
		}
		if dcm.VerificationURL != "" && dcm.UserCode != "" {
			msg = fmt.Sprintf("Please authenticate with Azure AD at: %s and enter the code: %s", dcm.VerificationURL, dcm.UserCode)
		}
		// Send the redirect url to the user who tries to authenticate.
		cfg.RLock()
		defer cfg.RUnlock()
		webex, err := newWebexCli(cfg.Webex.Token)
		if err != nil {
			return fmt.Errorf("error creating webex client: %w", err)
		}
		if err := webex.sendPlainMessage(ctx, username, msg); err != nil {
			return fmt.Errorf("error sending message to user: %w", err)
		}
		// Send another single message with the code to be able to copy it.
		if dcm.UserCode != "" {
			msg := webexMsg{
				ToPersonEmail: username,
				Markdown:      fmt.Sprintf("`%s`", dcm.UserCode),
			}
			if err := webex.sendMessage(ctx, msg); err != nil {
				return fmt.Errorf("error sending message to user: %w", err)
			}
		}
		log.Debugf("💬 successfully sent webex messages to user %s", username)
		return nil
	})
	if err != nil {
		return false, fmt.Errorf("error starting device code flow with Azure AD: %w", err)
	}

	// Now let's wait for the device code flow to finish.
	// This will block until the user has authenticated or the timeout is reached.
	// If the timeout is reached, the user will be denied access.
	log.Debugf("⏳ waiting for user %s to authenticate with Azure AD", username)
	_, err = deviceCred.GetToken(ctx, policy.TokenRequestOptions{
		Scopes: []string{"email"}, // Hardocded for now, we probably don't need more anytime soon.
	})
	if err != nil {
		return false, fmt.Errorf("error getting (waiting for) token from device code flow: %w", err)
	}

	// We made it.
	log.Infof("🔓 user %s successfully authenticated with Azure AD", username)

	// Let's try to send the trusted device token to the user. In case it errors
	// out, we just log it, but that's not a reason to deny access.
	if trustedToken != "" {
		go func() {
			// First we store the device token in the encryption store. To the
			// user we send the key used to encrypt the token in the store so
			// only they can decrypt it and we don't leak trusted device tokens
			// to (disk) storage.
			tdt, err := store.Put([]byte(trustedToken))
			if err != nil {
				log.Errorf("🤔 trusted token: error storing token: %v", err)
				return
			}
			cfg.RLock()
			defer cfg.RUnlock()
			// We use our own context here with a timeout of 5 minutes. The
			// other context would have already been cancelled by the time this
			// goroutine runs.
			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Minute)
			defer cancel()
			webex, err := newWebexCli(cfg.Webex.Token)
			if err != nil {
				log.Errorf("🤔 trusted token: error creating webex client: %v", err)
				return
			}
			msg := webexMsg{
				ToPersonEmail: username,
				Markdown: fmt.Sprintf(
					"Login successful, awesome 🥳\nYour trusted device token is posted below.\n\nUse it as the password next time instead of the DUO passcode.\n\nIt is valid for 30 days.\n\n```\n%s",
					tdt.Marshal(),
				),
			}
			if err := webex.sendMessage(ctx, msg); err != nil {
				log.Errorf("🤔 trusted token: error sending message to user: %v", err)
				return
			}
		}()
	}

	return true, nil
}

type deviceCodePromptFunc func(context.Context, azidentity.DeviceCodeMessage) error

// Start the device code flow with Azure AD and return the url and the device
// code so we can send it to the user.
func startAADDeviceCodeFlow(ctx context.Context, username string, prompt deviceCodePromptFunc) (*azidentity.DeviceCodeCredential, error) {
	// Get the tenant ID and client ID from the config.
	cfg.RLock()
	defer cfg.RUnlock()
	tenantID := cfg.AzureAD.TenantID
	clientID := cfg.AzureAD.ClientID

	// Create a new device code credential using the access token.
	cred, err := azidentity.NewDeviceCodeCredential(
		&azidentity.DeviceCodeCredentialOptions{
			ClientID:   clientID,
			TenantID:   tenantID,
			UserPrompt: prompt,
		})
	if err != nil {
		return nil, fmt.Errorf("error creating new device code credential: %w", err)
	}
	return cred, nil
}
