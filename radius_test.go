package main

import (
	"crypto/md5"
	"net"
	"os"
	"testing"

	"github.com/google/go-cmp/cmp"
)

var (
	testAuthenticator = []byte{0x37, 0x52, 0x0a, 0x29, 0xcb, 0xcd, 0x4e, 0xe1, 0x7b, 0x5a, 0xa5, 0x60, 0xff, 0x7f, 0x75, 0x99} // -|7R.)..N.{Z.`..u.|
	testUserPassword  = []byte{0x6f, 0x75, 0x74, 0x69, 0xb7, 0x0e, 0xdb, 0x4c, 0x77, 0x9b, 0x3f, 0x9e, 0x2b, 0x8d, 0x9b, 0x21} // Encrypted password
)

func TestParseRADIUSRequest(t *testing.T) {
	tests := []struct {
		name     string
		fixture  string
		expected *RADIUSPacket
		wantErr  bool
	}{
		{
			name:    "Access-Request",
			fixture: "testdata/access-request.bin",
			expected: &RADIUSPacket{
				Code:          CodeAccessRequest,
				Identifier:    46,
				Length:        83,
				Authenticator: testAuthenticator,
				Attributes: []RADIUSAttribute{
					{
						Type:   AttributeServiceType,
						Length: 6,
						Value:  []byte{0x00, 0x00, 0x00, 0x02},
					},
					{
						Type:   AttributeFramedProtocol,
						Length: 6,
						Value:  []byte{0x00, 0x00, 0x00, 0x01},
					},
					{
						Type:   AttributeUserName,
						Length: 10,
						Value:  []byte(`username`),
					},
					{
						Type:   AttributeUserPassword,
						Length: 18,
						Value:  testUserPassword,
					},
					{
						Type:   AttributeCallingStationID,
						Length: 11,
						Value:  []byte(`CLIENTVPN`),
					},
					{
						Type:   AttributeNASIPAddress,
						Length: 6,
						Value:  net.IPv4(6, 87, 24, 224).To4(),
					},
					{
						Type:   AttributeNASPort,
						Length: 6,
						Value:  []byte{0x00, 0x00, 0x00, 0x00},
					},
				},
			},
		},
		// This test should fail because the radius packet is too short.
		{
			name:     "short Access-Request",
			fixture:  "testdata/access-request-short.bin",
			expected: nil,
			wantErr:  true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Read the fixture from file.
			fixture, err := os.ReadFile(tt.fixture)
			if err != nil {
				t.Fatalf("error reading fixture: %v", err)
			}

			// Parse the fixture.
			actual, err := ParseRADIUSPacket(fixture)
			if (err != nil) != tt.wantErr {
				t.Errorf("error parsing RADIUS packet: %v, wantErr = %v", err, tt.wantErr)
				return
			}

			// Compare the actual and expected results.
			if tt.expected != nil {
				if diff := cmp.Diff(actual, tt.expected); diff != "" {
					t.Error(diff)
				}
			}
		})
	}
}

func TestDecryptPassword(t *testing.T) {
	tests := []struct {
		name          string
		authenticator []byte
		secret        []byte
		password      []byte
		expected      []byte
		expectedError error
	}{
		{
			name:          "Successful decryption",
			authenticator: testAuthenticator,
			secret:        []byte("Einfach1"),
			password:      testUserPassword,
			expected:      []byte("secret"),
			expectedError: nil,
		},
		{
			name:          "Invalid authenticator length",
			authenticator: []byte{0x3a, 0x5a, 0x76, 0x23, 0xdf, 0x20, 0x2a, 0x11, 0x42, 0x5b, 0xcf, 0x52, 0x5b, 0xbf, 0x57},
			secret:        []byte("Einfach1"),
			password:      []byte{0x9f, 0x6c, 0x7c, 0x2d, 0x9c, 0x4d, 0x9c, 0x9f, 0x9c, 0x9c, 0x9c, 0x9c, 0x9c, 0x9c, 0x9c, 0x9c},
			expected:      nil,
			expectedError: ErrInvalidAuthenticatorLength,
		},
		{
			name:          "Invalid password length",
			authenticator: testAuthenticator,
			secret:        []byte("Einfach1"),
			password:      []byte{0x9f, 0x6c, 0x7c, 0x2d, 0x9c, 0x4d, 0x9c, 0x9f, 0x9c, 0x9c, 0x9c, 0x9c, 0x9c, 0x9c, 0x9c},
			expected:      nil,
			expectedError: ErrInvalidPasswordLength,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			actual, err := decryptPassword(tt.authenticator, tt.secret, tt.password)
			if err != tt.expectedError {
				t.Errorf("unexpected error:\nexpected: %v\nactual:   %v", tt.expectedError, err)
			}
			if diff := cmp.Diff(actual, tt.expected); diff != "" {
				t.Error(diff)
				t.Errorf("actual = %q", actual)
			}
		})
	}
}

func TestComputeRA(t *testing.T) {
	tests := []struct {
		name         string
		pkt          *RADIUSPacket
		req          []byte
		secret       []byte
		expectedAuth []byte
	}{
		{
			name: "Successful computation",
			pkt: &RADIUSPacket{
				Code:          CodeAccessReject,
				Identifier:    43,
				Authenticator: nil,
				Attributes:    nil,
			},
			req:          []byte("\x6f\x75\xdf\x88\x62\x88\x8f\x90\xd4\xf6\xff\x19\xae\x4f\x38\xfb"),
			secret:       []byte("testing123"),
			expectedAuth: []byte("\x42\xd5\xfe\x68\x69\x9d\xe5\x32\x2f\x63\x6f\x56\x6b\xdd\x10\xf3"),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			actualAuth := tt.pkt.ComputeRA(tt.req, tt.secret)
			if diff := cmp.Diff(actualAuth, tt.expectedAuth); diff != "" {
				t.Error(diff)
			}
		})
	}
}

func TestXOR(t *testing.T) {
	tests := []struct {
		name     string
		a        []byte
		b        []byte
		expected []byte
	}{
		{
			name:     "Equal length inputs",
			a:        []byte{0x01, 0x02, 0x03},
			b:        []byte{0x04, 0x05, 0x06},
			expected: []byte{0x05, 0x07, 0x05},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			actual := xor(tt.a, tt.b)
			if diff := cmp.Diff(actual, tt.expected); diff != "" {
				t.Error(diff)
			}
		})
	}
}

func TestMd5Hash(t *testing.T) {
	tests := []struct {
		name     string
		input    []byte
		expected [16]byte
	}{
		{
			name:     "Empty input",
			input:    []byte{},
			expected: md5.Sum([]byte{}),
		},
		{
			name:     "Non-empty input",
			input:    []byte("test"),
			expected: md5.Sum([]byte("test")),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			actual := md5Hash(tt.input)
			if diff := cmp.Diff(actual, tt.expected[:]); diff != "" {
				t.Error(diff)
			}
		})
	}
}
