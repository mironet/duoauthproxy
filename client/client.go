package client

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"time"

	"golang.org/x/net/publicsuffix"
)

var (
	defaultUserAgent = "mironet-http-client"
	mediaType        = "application/json"
)

// Client is a Meraki API Client.
type Client struct {
	BaseURL       *url.URL      // Base URL.
	UserAgent     string        // User-Agent to use in HTTP request headers.
	MaxRetry      int           // Maximum retries before giving up refreshing a token.
	RetryInterval time.Duration // Interval used for auth token refresh retries.

	client            *http.Client
	authorization     string      // Authorization: header.
	additionalHeaders http.Header // Those headers are added automatically for each request.
}

// Opt is an option for the SFClient.
type Opt func(c *Client) error

// WithHTTPClient sets the http.Client on the API client.
func WithHTTPClient(cli *http.Client) Opt {
	return func(c *Client) error {
		c.client = cli
		return nil
	}
}

// WithAuthorization sets the Authorization header for this client. The header
// is injected on each request after setting it.
func WithAuthorization(key string) Opt {
	return func(c *Client) error {
		c.authorization = fmt.Sprintf("Bearer %s", key)
		return nil
	}
}

// WithAdditionalHeaders configures the client to set those headers on each
// request.
func WithAdditionalHeaders(he http.Header) Opt {
	return func(c *Client) error {
		for k, vv := range he {
			for _, v := range vv {
				c.additionalHeaders.Add(k, v)
			}
		}
		return nil
	}
}

// WithBaseURL parses and sets the base url for future requests with this
// client.
func WithBaseURL(rawurl string) Opt {
	return func(c *Client) error {
		u, err := url.Parse(rawurl)
		if err != nil {
			return err
		}
		c.BaseURL = u
		return nil
	}
}

// WithInsecure configures the client to not check TLS certificates.
func WithInsecure(insecure bool) Opt {
	return func(c *Client) error {
		c.client = newHTTPClient(insecure)
		return nil
	}
}

type teeTransport struct {
	http.RoundTripper
	interceptor io.Writer
}

// TeeReadCloser returns a Reader that writes to w what it reads from r.
func TeeReadCloser(r io.ReadCloser, w io.Writer) io.ReadCloser {
	return &teeReadCloser{r, w}
}

type teeReadCloser struct {
	r io.ReadCloser
	w io.Writer
}

func (t *teeReadCloser) Read(p []byte) (n int, err error) {
	n, err = t.r.Read(p)
	if n > 0 {
		if n, err := t.w.Write(p[:n]); err != nil {
			return n, err
		}
	}
	return
}

func (t *teeReadCloser) Close() error {
	return t.r.Close()
}

func (t *teeTransport) RoundTrip(r *http.Request) (*http.Response, error) {
	r.Body = TeeReadCloser(r.Body, t.interceptor)
	return t.RoundTripper.RoundTrip(r)
}

// WithIntercept sets the client in a mode to output all traffic send to and
// from it to the out writer.
func WithIntercept(out io.Writer) Opt {
	return func(c *Client) error {
		if out == nil {
			return nil
		}
		c.client.Transport = &teeTransport{
			RoundTripper: c.client.Transport,
			interceptor:  out,
		}
		return nil
	}
}

// See https://blog.cloudflare.com/the-complete-guide-to-golang-net-http-timeouts/
func newHTTPClient(insecure bool) *http.Client {
	c := &http.Client{
		Transport: &http.Transport{
			Dial: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
			}).Dial,
			TLSHandshakeTimeout:   30 * time.Second,
			ResponseHeaderTimeout: 30 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
			IdleConnTimeout:       90 * time.Second, // This is the default.
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: insecure,
			},
		},
	}
	return c
}

// New returns a new HTTP client instance.
func New(opts ...Opt) (*Client, error) {
	cookieJar, err := cookiejar.New(&cookiejar.Options{PublicSuffixList: publicsuffix.List})
	if err != nil {
		return nil, fmt.Errorf("error creating cookie jar: %v", err)
	}

	c := newHTTPClient(false)
	c.Jar = cookieJar

	cli := &Client{
		//BaseURL:       base,
		client:        c,
		UserAgent:     defaultUserAgent,
		MaxRetry:      3,
		RetryInterval: time.Second * 30,
	}

	for _, opt := range opts {
		if err := opt(cli); err != nil {
			return nil, err
		}
	}

	return cli, nil
}

// Client gives access to the internal HTTP client.
func (c *Client) Client() *http.Client {
	return c.client
}

// NewRequest prepares a new request for sending over to the server. Specify
// relative urls always without the preceding slash. 'body' is JSON-encoded and
// sent as the request body. If body is an io.Reader its content is copied
// without JSON parsing.
func (c *Client) NewRequest(ctx context.Context, method, urlStr string, body interface{}, opts ...Opt) (*http.Request, error) {
	for _, opt := range opts {
		if err := opt(c); err != nil {
			return nil, err
		}
	}

	var u *url.URL
	{
		rel, err := url.Parse(urlStr)
		if err != nil {
			return nil, err
		}
		u = c.BaseURL.ResolveReference(rel)
	}

	var (
		buf = new(bytes.Buffer)
		req *http.Request
		err error
	)

	if body != nil {
		// If body is an io.Reader we can use it directly, so don't encode.
		if v, ok := body.(io.Reader); !ok {
			if err := json.NewEncoder(buf).Encode(body); err != nil {
				return nil, err
			}
			req, err = http.NewRequest(method, u.String(), buf)
		} else {
			req, err = http.NewRequest(method, u.String(), v)
		}
	} else {
		req, err = http.NewRequest(method, u.String(), nil)
	}
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", mediaType)
	req.Header.Add("Accept", mediaType)
	req.Header.Add("User-Agent", c.UserAgent)

	if c.authorization != "" {
		req.Header.Add("Authorization", c.authorization)
	}

	// Mix in additional headers.
	for k, vv := range c.additionalHeaders {
		for _, v := range vv {
			req.Header.Add(k, v)
		}
	}

	return req.WithContext(ctx), nil
}

// Do actually sends the request and stores the response body in v. If `v` is an
// `io.Writer` the contents of the response body are copied without JSON
// parsing.
func (c *Client) Do(ctx context.Context, req *http.Request, v interface{}) (*http.Response, error) {
	resp, err := c.doRequestWithClient(ctx, c.client, req)
	if err != nil {
		return nil, err
	}

	err = checkResponse(resp)
	if err != nil {
		return nil, fmt.Errorf("error while requesting resource via HTTP: resp: %v, err: %w", resp, err)
	}

	defer func() {
		// Drain the body in case there's something in it, but we're not
		// interested.
		io.Copy(io.Discard, resp.Body)
		// Always close the body from the response.
		resp.Body.Close()
	}()

	if v == nil {
		return resp, nil
	}

	if w, ok := v.(io.Writer); ok {
		_, err = io.Copy(w, resp.Body)
		if err != nil {
			return nil, fmt.Errorf("error while reading body from response: %w", err)
		}

		return resp, nil
	}

	err = json.NewDecoder(resp.Body).Decode(v) // Unmarshal JSON into v.
	if err != nil {
		var buf bytes.Buffer
		buf.ReadFrom(resp.Body)

		return nil, fmt.Errorf("error while decoding json %s: %w", buf.String(), err)
	}

	return resp, nil
}

// doRequestWithClient sends to request over the wire.
func (c *Client) doRequestWithClient(ctx context.Context, client *http.Client, req *http.Request) (*http.Response, error) {
	req = req.WithContext(ctx)

	resp, err := c.client.Do(req)
	if err != nil {
		// This is a Layer 1 to 4 error, not HTTP error.
		// It could also be the context that has been closed.
		return nil, fmt.Errorf("error while sending request via HTTP: %v", err)
	}

	return resp, nil
}

// ErrorResponse is an error sent back from the API.
type ErrorResponse struct {
	Response *http.Response `json:"-"`
	Message  string         `json:"message"`
	Errors   []string       `json:"errors"`
}

// Error returns the string representation of the error.
func (e *ErrorResponse) Error() string {
	return fmt.Sprintf("error from API: %s %s: %d %v",
		e.Response.Request.Method,
		e.Response.Request.URL,
		e.Response.StatusCode,
		e.Message,
	)
}

// CheckResponse checks the HTTP status code of the response and if it's in the
// 200s the request is considered error-free. In case it's not, the body will be
// read and parsed.
func checkResponse(r *http.Response) error {
	if c := r.StatusCode; c >= 200 && c <= 299 {
		return nil
	}

	errorResponse := &ErrorResponse{Response: r}
	data, err := io.ReadAll(r.Body) // Drain to reuse.
	defer r.Body.Close()
	if err != nil || len(data) == 0 {
		return errorResponse
	}

	err = json.Unmarshal(data, errorResponse)
	if err != nil {
		errorResponse.Message = string(data)
	}

	return errorResponse
}
