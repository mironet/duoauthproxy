package main

import (
	"fmt"
	"net"
	"os"

	"github.com/urfave/cli/v2"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var (
	log  *zap.SugaredLogger
	atom zap.AtomicLevel = zap.NewAtomicLevel()
	// The encryption store for trusted device tokens.
	store encryptionStore = newMemoryEncryptionStore()

	version     = "development"
	commitHash  = ""
	compileDate = ""
)

func init() {
	version = fmt.Sprintf("%s-%s (%s)", version, commitHash, compileDate)
}

func initLogging() func() error {
	// Initialize the logger.
	encodercfg := zap.NewDevelopmentEncoderConfig()
	logger := zap.New(zapcore.NewCore(
		zapcore.NewConsoleEncoder(encodercfg),
		zapcore.Lock(os.Stdout),
		atom,
	))
	log = logger.Sugar()
	return logger.Sync
}

func main() {
	teardown := initLogging()
	defer teardown()

	app := &cli.App{
		Name:    "radius-server",
		Usage:   "A server listening for RADIUS requests to forward to Azure AD and then DUO.",
		Version: version,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:    "address",
				Usage:   "The address to listen on.",
				Value:   ":1812",
				Aliases: []string{"a"},
			},
			// String flag for a secrets config file.
			&cli.StringFlag{
				Name:    "config",
				Usage:   "The path to a secrets config file.",
				Value:   "config.yml",
				Aliases: []string{"c"},
			},
			// Flag controlling the log level (debug, info, warn, error).
			&cli.StringFlag{
				Name:    "log-level",
				Usage:   "The log level to use.",
				Value:   "info",
				Aliases: []string{"l"},
			},
			// Flag for the Azure AD tenant ID.
			&cli.StringFlag{
				Name:    "tenant-id",
				Usage:   "The Azure AD tenant ID.",
				Value:   "",
				Aliases: []string{"t"},
				EnvVars: []string{"AZURE_TENANT_ID"},
			},
			// Flag for the Azure AD client ID.
			&cli.StringFlag{
				Name:    "client-id",
				Usage:   "The Azure AD client ID.",
				Value:   "",
				Aliases: []string{"i"},
				EnvVars: []string{"AZURE_CLIENT_ID"},
			},
		},
		Action: func(c *cli.Context) error {
			// Set the log level.
			logLevel := c.String("log-level")
			switch logLevel {
			case "debug":
				atom.SetLevel(zap.DebugLevel)
			case "info":
				atom.SetLevel(zap.InfoLevel)
			case "warn":
				atom.SetLevel(zap.WarnLevel)
			default:
				atom.SetLevel(zap.InfoLevel)
			}

			log.Infof("🤖 server version %s", version)

			// Read the config file.
			configFile := c.String("config")
			log.Infof("⚙️ reading config from %s ...", configFile)
			if err := readConfig(configFile); err != nil {
				log.Fatal(err)
			}
			log.Infof("⚙️ loaded %d clients", len(cfg.GetClients()))

			// If the Azure AD tenant ID and/or client ID have been set in the
			// flags, set them in the config struct.
			if tenantID := c.String("tenant-id"); tenantID != "" {
				cfg.SetAADTenantID(tenantID)
			}
			if clientID := c.String("client-id"); clientID != "" {
				cfg.SetAADClientID(clientID)
			}
			// Check the config for validity.
			if err := cfg.IsValid(); err != nil {
				return fmt.Errorf("😱 invalid config: %w", err)
			}

			// Use the redis store in case it is configured.
			if cfg.Redis.Addr != "" {
				log.Infof("📦 using redis store at %s", cfg.Redis.Addr)
				var err error
				store, err = newRedisEncryptionStore(c.Context, cfg.Redis.Addr, cfg.Redis.Password)
				if err != nil {
					return fmt.Errorf("😱 error initializing redis connection: %w", err)
				}
			}

			// Start the RADIUS server.

			address := c.String("address")
			// Create a UDP listener on port 1812
			log.Infof("🐕 starting RADIUS server on %s", address)
			addr, err := net.ResolveUDPAddr("udp", address)
			if err != nil {
				log.Fatal(err)
			}
			sock, err := net.ListenUDP("udp", addr)
			if err != nil {
				log.Fatal(err)
			}
			defer sock.Close()

			// Handle incoming RADIUS requests
			handleRADIUSRequests(sock)
			return nil
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
