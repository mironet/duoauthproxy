# Duoauthproxy

Tiny RADIUS server for Meraki Client VPN to use in conjuction with DUO and Azure
AD device authorization grant flow.

## Login flow

The user logs in by sending the current DUO passcode shown in the authenticator
app on the mobile device in the password field when connecting to Meraki Client
VPN.

If that checks out with DUO, the Azure AD device authorization grant flow is
started. The user will receive a URL via Webex to visit, authenticate and
confirm the client vpn login.

Upon successful authentication the user is sent a new password via Webex to use
from now on when connecting to Meraki Client VPN. It is valid for a longer time.
A login with this password still triggers a DUO push request though.

## Configuration

The config struct in `config.go` defines the overall configuration for the
duoauthproxy server. Here's an example YAML configuration file that shows how to
configure the duoauthproxy server using the config struct:

```yaml
azuread:
    tenant_id: "myazuretenantid"
    client_id: "myazureclientid"
clients:
    - ip: "192.168.1.1"
        secret: "myradiussecret"
    - ip: "192.168.1.2"
        secret: "myradiussecret"
duo:
    integration_key: "myduointegrationkey"
    secret_key: "myduosecretkey"
    api_hostname: "api-XXXXXXXX.duosecurity.com"
webex:
    token: "mywebexaccesstoken"
redis:
    addr: "redis:6379"
    password: ""
```

In this example, we define the following configuration options:

* `azuread`: The Azure AD configuration options, including the tenant ID, client
  ID. This is something you need to create in Azure AD beforehand. Register a
  new app and allow public client flows. Nothing else needs to be set for it to
  work although you might want to restrict access to the app to certain users or
  groups and configure branding.

* `clients`: An array of RADIUS client configurations, including the IP address
      and shared secret for each client. From RFC 2865: The secret (password
      shared between the client and the RADIUS server) SHOULD be at least as
      large and unguessable as a well- chosen password.  It is preferred that
      the secret be at least 16 octets.  This is to ensure a sufficiently large
      range for the secret to provide protection against exhaustive search
      attacks.

* `duo`: The DUO configuration options, including the integration key, secret
  key, and API hostname. You can get this information when you register a
  "protected app" in the admin dashboard.

* `webex`: The Webex Teams configuration options, including the access token for
  the bot.

* `redis`: The Redis configuration options, including the address and password
  for the Redis server.

## Security Considerations

### Login Flow

Several security considerations were taken into account when designing the login
flow. The concept relies on two steps roughly:

1. Initial login with DUO passcode and Azure AD device authorization grant flow
   to get a trusted device token.
2. Subsequent logins with trusted device token and DUO push request.

The first step is required to get a trusted device token. The second step is
required to get a DUO push request. The trusted device token is not used to
bypass DUO, but to identify the user and send a DUO push request to the user's
device. The user still needs to approve the push request to get access.

Since we send the trusted device token to the user via Webex Teams, out-of-band
communication of tokens is done securely. Webex is end-to-end encrypted and the
token is encrypted with AES-256-GCM. The key and nonce are sent to the user via
Webex Teams and the token is stored in redis. The key and nonce are not stored
anywhere and are only used to decrypt the token when the user submits them.

### Storage of Trusted Device Tokens

The trusted device token feature of DUO (remember devices) is somewhat abused in
this setup. The token is not used to bypass DUO, but in fact to use as a primary
authentication measure for the RADIUS server. The token is stored in a Redis
database and used to authenticate the user via DUO. According to DUO, the idea
is for the app not to ask for a 2nd factor anymore, but let the user in directly
(without TFA). In our case though, the token is used to authenticate the user,
but the 2nd factor is still required. The token is only used to identify the
user and to send a DUO push request to the user's device.

### Encryption of Trusted Device Tokens

The device tokens are stored in redis. They are usually too long for a password
in a RADIUS `Access-Request` packet (>128 bytes). The tokens are encrypted
before storing them in redis (with AES-256-GCM) and the generated key and nonce
is base64-encoded and sent to the user via Webex Teams. The wire format can be
inspected in `crypto.go`. When the user submits this key and nonce, the token
can be decrypted and used to authenticate the user in DUO. This way the redis
database does not contain any plaintext tokens and the tokens are also not sent
to the user at any time. Plus we work around the 128 byte limit for passwords in
RADIUS.