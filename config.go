package main

import (
	"fmt"
	"os"
	"sync"

	"gopkg.in/yaml.v2"
)

var (
	// Central object for the configuration.
	cfg config
)

type config struct {
	sync.RWMutex
	AzureAD azureADConfig  `yaml:"azuread,omitempty"`
	Clients []radiusClient `yaml:"clients,omitempty"`
	Duo     duoConfig      `yaml:"duo,omitempty"`
	Webex   webexConfig    `yaml:"webex,omitempty"`
	Redis   redisConfig    `yaml:"redis,omitempty"`
}

type duoConfig struct {
	IntegrationKey string `yaml:"integration_key"`
	SecretKey      string `yaml:"secret_key"`
	APIHostname    string `yaml:"api_hostname"`
}

type webexConfig struct {
	Token string `yaml:"token,omitempty"`
}

type redisConfig struct {
	Addr     string `yaml:"addr,omitempty"`
	Password string `yaml:"password,omitempty"`
}

type radiusClient struct {
	IP     string `yaml:"ip,omitempty"`
	Secret string `yaml:"secret,omitempty"`
}

type azureADConfig struct {
	TenantID string `yaml:"tenant_id,omitempty"`
	ClientID string `yaml:"client_id,omitempty"`
}

// Read the secrets config file.
func readConfig(path string) error {
	cfg.Lock()
	defer cfg.Unlock()
	file, err := os.ReadFile(path)
	if err != nil {
		return err
	}
	if err := yaml.Unmarshal(file, &cfg); err != nil {
		return err
	}
	return nil
}

// Return a shared secret of a radius client based on the source IP of the
// access-request packet.
func (c *config) SharedSecret(sip string) ([]byte, bool) {
	c.RLock()
	defer c.RUnlock()
	for _, v := range c.Clients {
		if v.IP == sip {
			return []byte(v.Secret), true
		}
	}
	return nil, false
}

func (c *config) GetClients() []radiusClient {
	c.RLock()
	defer c.RUnlock()
	return c.Clients
}

func (c *config) SetAADTenantID(tenant string) {
	c.Lock()
	defer c.Unlock()
	c.AzureAD.TenantID = tenant
}

func (c *config) SetAADClientID(client string) {
	c.Lock()
	defer c.Unlock()
	c.AzureAD.ClientID = client
}

func (c *config) IsValid() error {
	c.RLock()
	defer c.RUnlock()
	if c.AzureAD.TenantID == "" || c.AzureAD.ClientID == "" {
		return fmt.Errorf("azuread.tenant_id and azuread.client_id must be set")
	}
	if len(c.Clients) == 0 {
		return fmt.Errorf("no RADIUS clients configured")
	}
	if c.Duo.IntegrationKey == "" || c.Duo.SecretKey == "" || c.Duo.APIHostname == "" {
		return fmt.Errorf("duo integration_key, secret_key and api_hostname must be set")
	}
	if c.Webex.Token == "" {
		return fmt.Errorf("webex token must be set")
	}
	return nil
}
