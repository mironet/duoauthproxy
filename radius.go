package main

import (
	"bytes"
	"crypto/md5"
	"encoding/binary"
	"fmt"
	"unicode/utf8"
)

type RADIUSCode byte

const (
	CodeAccessRequest RADIUSCode = 1
	CodeAccessAccept  RADIUSCode = 2
	CodeAccessReject  RADIUSCode = 3
)

func (code RADIUSCode) String() string {
	switch code {
	case CodeAccessRequest:
		return "Access-Request"
	case CodeAccessAccept:
		return "Access-Accept"
	case CodeAccessReject:
		return "Access-Reject"
	}
	return fmt.Sprintf("Unknown RADIUS code: %d", code)
}

type RADIUSType byte

const (
	TypePPP RADIUSType = 1
)

type RADIUSServiceType uint32

const (
	ServiceTypeFramed RADIUSServiceType = 2
)

type RADIUSFramedProtocol uint32

const (
	FramedProtocolPPP RADIUSFramedProtocol = 1
)

type RADIUSAttributeType byte

const (
	AttributeUserName         RADIUSAttributeType = 1
	AttributeUserPassword     RADIUSAttributeType = 2
	AttributeNASIPAddress     RADIUSAttributeType = 4
	AttributeNASPort          RADIUSAttributeType = 5
	AttributeServiceType      RADIUSAttributeType = 6
	AttributeFramedProtocol   RADIUSAttributeType = 7
	AttributeReplyMessage     RADIUSAttributeType = 18
	AttributeCallingStationID RADIUSAttributeType = 31
)

var radiusAttributes map[RADIUSAttributeType]string = map[RADIUSAttributeType]string{
	AttributeUserName:         "User-Name",
	AttributeUserPassword:     "User-Password",
	AttributeNASIPAddress:     "NAS-IP-Address",
	AttributeNASPort:          "NAS-Port",
	AttributeServiceType:      "Service-Type",
	AttributeFramedProtocol:   "Framed-Protocol",
	AttributeReplyMessage:     "Reply-Message",
	AttributeCallingStationID: "Calling-Station-ID",
}

func (attr RADIUSAttributeType) String() string {
	if s, ok := radiusAttributes[attr]; ok {
		return s
	}
	return fmt.Sprintf("Unknown RADIUS attribute type: %d", attr)
}

type RADIUSAttribute struct {
	Type   RADIUSAttributeType
	Length int
	Value  []byte
}

func (attr *RADIUSAttribute) String() string {
	if utf8.Valid(attr.Value) {
		return fmt.Sprintf("AVP: t=%s (%d), l=%d, v=%s", attr.Type, attr.Type, attr.Length, string(attr.Value))
	}
	return fmt.Sprintf("AVP: t=%s (%d), l=%d, v=%v", attr.Type, attr.Type, attr.Length, attr.Value)
}

// 0                   1                   2                   3
// 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// |     Code      |  Identifier   |            Length             |
// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// |                                                               |
// |                         Authenticator                         |
// |                                                               |
// |                                                               |
// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// |  Attributes ...
// +-+-+-+-+-+-+-+-+-+-+-+-+-

type RADIUSPacket struct {
	Code          RADIUSCode
	Identifier    byte
	Length        int
	Authenticator []byte
	Attributes    []RADIUSAttribute
}

// String outputs the RADIUS packet in a human readable format.
func (pkt *RADIUSPacket) String() string {
	pkt.Length = pkt.Len()
	var buf bytes.Buffer
	buf.WriteString(fmt.Sprintf("Code: %s (%d)\n", pkt.Code, pkt.Code))
	buf.WriteString(fmt.Sprintf("Identifier: %#x (%d)\n", pkt.Identifier, pkt.Identifier))
	buf.WriteString(fmt.Sprintf("Length: %d\n", pkt.Length))
	buf.WriteString(fmt.Sprintf("Authenticator: %x\n", pkt.Authenticator))
	buf.WriteString("Attribute Value Pairs:\n")
	for _, attr := range pkt.Attributes {
		buf.WriteString(fmt.Sprintf("	%s\n", attr.String()))
	}
	return buf.String()
}

// Calculate the length of the RADIUS packet.
func (pkt *RADIUSPacket) Len() int {
	length := 20
	for _, attr := range pkt.Attributes {
		length += 2 + len(attr.Value)
	}
	return length
}

// Compute the response authenticator field, see RFC 2865 section 3.
func (pkt *RADIUSPacket) ComputeRA(reqAuth, secret []byte) []byte {
	tmp := &RADIUSPacket{
		Code:          pkt.Code,
		Identifier:    pkt.Identifier,
		Authenticator: reqAuth,
		Attributes:    pkt.Attributes,
	}
	bb := tmp.Marshal()
	bb = append(bb, secret...)
	return md5Hash(bb)
}

// Marshal the RADIUS packet into the wire reprensentation.
func (pkt *RADIUSPacket) Marshal() []byte {
	pkt.Length = pkt.Len()
	buf := make([]byte, 20)
	buf[0] = byte(pkt.Code)
	buf[1] = pkt.Identifier
	binary.BigEndian.PutUint16(buf[2:4], uint16(pkt.Length))
	copy(buf[4:20], pkt.Authenticator)
	for _, attr := range pkt.Attributes {
		attrBuf := make([]byte, 2+len(attr.Value))
		attrBuf[0] = byte(attr.Type)
		attrBuf[1] = byte(len(attrBuf))
		copy(attrBuf[2:], attr.Value)
		buf = append(buf, attrBuf...)
	}
	return buf
}

func ParseRADIUSPacket(buf []byte) (*RADIUSPacket, error) {
	if len(buf) < 20 {
		return nil, fmt.Errorf("RADIUS packet too short")
	}
	pkt := &RADIUSPacket{
		Code:          RADIUSCode(buf[0]),
		Identifier:    buf[1],
		Length:        int(binary.BigEndian.Uint16(buf[2:4])),
		Authenticator: make([]byte, 16),
	}
	copy(pkt.Authenticator, buf[4:20])
	if pkt.Length > len(buf) {
		return nil, fmt.Errorf("RADIUS packet too long")
	}
	attrs, err := ParseRADIUSAttributes(buf[20:pkt.Length])
	if err != nil {
		return nil, err
	}
	pkt.Attributes = attrs
	return pkt, nil
}

func ParseRADIUSAttributes(buf []byte) ([]RADIUSAttribute, error) {
	var attrs []RADIUSAttribute
	for len(buf) > 0 {
		if len(buf) < 2 {
			return nil, fmt.Errorf("RADIUS attribute too short")
		}
		attr := RADIUSAttribute{
			Type:   RADIUSAttributeType(buf[0]),
			Length: int(buf[1]),
		}
		if attr.Length > len(buf) {
			return nil, fmt.Errorf("RADIUS attribute too long")
		}
		attr.Value = buf[2:attr.Length]
		attrs = append(attrs, attr)
		buf = buf[attr.Length:]
	}
	return attrs, nil
}

var (
	ErrInvalidAuthenticatorLength = fmt.Errorf("authenticator not 16 bytes")
	ErrInvalidPasswordLength      = fmt.Errorf("invalid password length")
)

// https://datatracker.ietf.org/doc/html/rfc2865#section-5.2
func decryptPassword(authenticator, secret, password []byte) ([]byte, error) {
	// We fail if the password is too long.
	if len(password) > 128 {
		return nil, ErrInvalidPasswordLength
	}
	// We also fail if the password is too short.
	if len(password) < 16 {
		return nil, ErrInvalidPasswordLength
	}
	// We also fail if the password is not a multiple of 16 bytes.
	if len(password)%16 != 0 {
		return nil, ErrInvalidPasswordLength
	}
	// We also fail if the authenticator is not 16 bytes.
	if len(authenticator) != 16 {
		return nil, ErrInvalidAuthenticatorLength
	}
	// Break the encrypted password into 16 byte chunks.
	chunks := make([][]byte, len(password)/16)
	for i := 0; i < len(password); i += 16 {
		chunks[i/16] = password[i : i+16]
	}
	// XOR each chunk with the md5 hash of the secret and the authenticator or
	// the previous chunk.
	pass := make([][]byte, len(chunks))
	for i := 0; i < len(chunks); i++ {
		var bi []byte
		if i == 0 {
			bi = append(secret, authenticator...)
		} else {
			bi = append(secret, chunks[i-1]...)
		}
		pass[i] = xor(chunks[i], md5Hash(bi))
	}

	// Concatenate the chunks removing the null padding.
	return bytes.Trim(bytes.Join(pass, nil), "\x00"), nil
}

// XOR two byte slices. Panics if the length of the slices is not equal.
func xor(a, b []byte) []byte {
	if len(a) != len(b) {
		panic("xor: length mismatch")
	}
	c := make([]byte, len(a))
	for i := range a {
		c[i] = a[i] ^ b[i]
	}
	return c
}

// Compute the md5 hash of a byte slice.
func md5Hash(buf []byte) []byte {
	hash := md5.Sum(buf)
	return hash[:]
}
