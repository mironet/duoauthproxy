package main

import (
	"context"
	"errors"
	"fmt"
	"net"
	"sync"
	"time"
)

var (
	// This keeps track of the connections that are currently being handled. If
	// maps source addresses (IP:port) to a list of bytes (identifiers).
	conntrack     = make(map[string][]byte)
	conntrackLock sync.RWMutex // Protects conntrack.
)

func handleRADIUSRequests(conn *net.UDPConn) {
	for {
		buf := make([]byte, 4096)
		n, addr, err := conn.ReadFromUDP(buf)
		if err != nil {
			log.Warnf("😱 error reading from UDP: %v", err)
			continue
		}

		// Check if we have a shared secret for this client.
		secret, ok := cfg.SharedSecret(addr.IP.String())
		if !ok {
			log.Warnf("😨 no shared secret for client: %s", addr.IP.String())
			continue
		}

		// Parse the RADIUS request
		req, err := ParseRADIUSPacket(buf[:n])
		if err != nil {
			log.Warnf("😱 error parsing RADIUS request: %v", err)
			continue
		}

		go func() {
			log.Infof("🔌 new RADIUS request from %s: %s", addr.String(), req.Code.String())
			log.Debugf("🔬 dumping RADIUS request from %s: %s", addr.String(), req.String())
			// Check if we are already handling a request from this client.
			if checkConntrack(addr, req.Identifier) {
				log.Warnf("✋ already handling request %#x (%d) from %s", req.Identifier, req.Identifier, addr.String())
				return
			}
			// Add the client to the connection tracker.
			addConntrack(addr.String(), req.Identifier)
			defer deleteConntrack(addr, req.Identifier)
			// Handle the request.
			if err := handleRequest(conn, addr, req, secret); err != nil {
				log.Errorf("😱 error handling RADIUS request: %v", err)
				return
			}
			log.Debugf("🔚 finished handling of RADIUS request from %s", addr.String())
		}()
	}
}

// Add the identifier to the connection tracker.
func addConntrack(addr string, id byte) {
	conntrackLock.Lock()
	defer conntrackLock.Unlock()
	conntrack[addr] = append(conntrack[addr], id)
}

// Check if the identifier is already in the connection tracker.
func checkConntrack(addr *net.UDPAddr, id byte) bool {
	conntrackLock.RLock()
	defer conntrackLock.RUnlock()
	if ids, ok := conntrack[addr.String()]; ok {
		for _, v := range ids {
			if v == id {
				return true
			}
		}
	}
	return false
}

// Remove the identifier from the connection tracker.
func deleteConntrack(addr *net.UDPAddr, id byte) {
	conntrackLock.Lock()
	defer conntrackLock.Unlock()
	if ids, ok := conntrack[addr.String()]; ok {
		for i, v := range ids {
			if v == id {
				ids = append(ids[:i], ids[i+1:]...)
				conntrack[addr.String()] = ids
				return
			}
		}
	}
}

func handleRequest(conn *net.UDPConn, addr *net.UDPAddr, req *RADIUSPacket, secret []byte) error {
	var reject = func(msg string) error {
		return sendAccessReject(conn, addr, req, secret, msg)
	}
	var accept = func() error {
		return sendAccessAccept(conn, addr, req, secret)
	}
	var username, password string
	for _, attr := range req.Attributes {
		if attr.Type == AttributeUserName {
			username = string(attr.Value)
			continue
		}
		if attr.Type == AttributeUserPassword {
			pw, err := decryptPassword(req.Authenticator, secret, attr.Value)
			if err != nil {
				return sendAccessReject(conn, addr, req, secret, "Error decrypting password")
			}
			password = string(pw)
			continue
		}
	}
	if username == "" || password == "" {
		return reject("Missing username or password")
	}

	// Set a timeout for the authentication, more a safety net to terminate this
	// goroutine at some point.
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Minute)
	defer cancel()
	// Get the IP address from the NAS-IP-Address attribute.
	var ip net.IP
	for _, attr := range req.Attributes {
		if attr.Type == AttributeNASIPAddress {
			if len(attr.Value) < 4 {
				return reject("Invalid NAS-IP-Address")
			}
			ip = net.IP(attr.Value)
			break
		}
	}
	ok, err := authenticateUser(ctx, username, password, ip.String())
	if err != nil {
		// Check if it's an external error. If so, we send the full error
		// message back to the client.
		var e errExternal
		if errors.As(err, &e) {
			// If there is a reason, send it back to the client.
			if e.Reason() != "" {
				log.Errorf("🔑 authentication failed: %v", e)
				return reject(e.Reason())
			}
			// If not, just send the error message.
			log.Errorf("🔑 authentication failed: %v", e)
			return reject(err.Error())
		}
		// Log the full (internal) error message, but don't send it along.
		log.Errorf("%v", err)
		return reject("Internal error")
	}
	if !ok {
		return reject("Authentication failed") // No further reason.
	}
	return accept()
}

func sendAccessAccept(conn *net.UDPConn, addr *net.UDPAddr, req *RADIUSPacket, secret []byte) error {
	resp := &RADIUSPacket{
		Code:       CodeAccessAccept,
		Identifier: req.Identifier,
	}
	resp.Authenticator = resp.ComputeRA(req.Authenticator, secret)
	respBytes := resp.Marshal()
	if _, err := conn.WriteToUDP(respBytes, addr); err != nil {
		return fmt.Errorf("error writing RADIUS response: %w", err)
	}
	log.Infof("✅ sent RADIUS response to %s: %s", addr.String(), resp.Code.String())
	log.Debugf("🔬 dumping RADIUS response to %s: %s", addr.String(), resp.String())
	return nil
}

func sendAccessReject(conn *net.UDPConn, addr *net.UDPAddr, req *RADIUSPacket, secret []byte, msg string) error {
	msgbytes := []byte(msg)
	resp := &RADIUSPacket{
		Code:       CodeAccessReject,
		Identifier: req.Identifier,
		Attributes: []RADIUSAttribute{
			{
				Type:   AttributeReplyMessage,
				Length: len(msgbytes) + 2,
				Value:  msgbytes,
			},
		},
	}
	resp.Authenticator = resp.ComputeRA(req.Authenticator, secret)
	respBytes := resp.Marshal()
	if _, err := conn.WriteToUDP(respBytes, addr); err != nil {
		return fmt.Errorf("error writing RADIUS response: %w", err)
	}
	log.Infof("⛔ sent RADIUS response to %s: %s", addr.String(), resp.Code.String())
	log.Debugf("🔬 dumping RADIUS response to %s: %s", addr.String(), resp.String())
	return nil
}
