package main

import (
	cryptorand "crypto/rand"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMarshal(t *testing.T) {
	tests := []struct {
		name string
		key  tdtKey
		want []byte
	}{
		{
			name: "non-empty key",
			key: tdtKey{
				ID:    "zAStuYdzxWbeJQn+",
				Nonce: []byte("tqckbcM5qqyx"),
				Key:   []byte("wWLVwrNSjqr6lY2twnEO1yTVNuUDXkq+"),
			},
			want: []byte("zAStuYdzxWbeJQn+dHFja2JjTTVxcXl4d1dMVndyTlNqcXI2bFkydHduRU8xeVRWTnVVRFhrcSs="),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.key.Marshal()
			if diff := cmp.Diff(got, tt.want); diff != "" {
				t.Error(diff)
			}
		})
	}
}

func TestMarshalUnmarshal(t *testing.T) {
	nonce := make([]byte, 12)
	if _, err := cryptorand.Read(nonce); err != nil {
		t.Fatal(err)
	}
	key := make([]byte, 32)
	if _, err := cryptorand.Read(key); err != nil {
		t.Fatal(err)
	}
	id := "zAStuYdzxWbeJQn+"

	testkey := tdtKey{
		ID:    id,
		Nonce: nonce,
		Key:   key,
	}

	// Now marshal and unmarshal the key and check if the unmarshaled key is equal to the original key.
	marshaled := testkey.Marshal()
	t.Logf("marshaled key (len=%d): %s", len(marshaled), marshaled)
	var unmarshaled tdtKey
	if err := unmarshaled.Unmarshal(marshaled); err != nil {
		t.Fatal(err)
	}
	if diff := cmp.Diff(testkey, unmarshaled); diff != "" {
		t.Error(diff)
	}
}
